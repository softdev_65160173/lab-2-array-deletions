public class ArrayRemoveElementLab {

    public static int removeElement(int[] nums, int val) {
        int k = 0; // จำนวนสมาชิกที่ไม่เท่ากับ val
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }

    public static void main(String[] args) {
        int[] nums1 = {3, 2, 2, 3};
        int val1 = 3;

        System.out.print("Input: nums = ");
        for(int i=0;i<nums1.length;i++) {
            System.out.print(nums1[i]+" ");
        }
        System.out.print(" val = "+val1);
        System.out.println();

        int k1 = removeElement(nums1, val1);
        System.out.print("Output: " + k1 + ", nums = [");
        for (int i = 0; i < k1; i++) {
            System.out.print(nums1[i]);
            if (i < k1 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");

        int[] nums2 = {0, 1, 2, 2, 4, 0, 3, 2};
        int val2 = 2;

        System.out.print("Input: nums = ");
        for(int i=0;i<nums2.length;i++) {
            System.out.print(nums2[i]+" ");
        }
        System.out.print(" val = "+val2);
        System.out.println();

        int k2 = removeElement(nums2, val2);
        System.out.print("Output: " + k2 + ", nums = [");
        for (int i = 0; i < k2; i++) {
            System.out.print(nums2[i]);
            if (i < k2 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
